package com.sigmotoa.codes.workshop;
/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Geometric Exercises
 */


import static java.lang.Math.*;

public class Geometric {

//Calculate the area for a square using one side
public static int squareArea(int side)
{
    int area;
    area = side*side;
    return area;
}

//Calculate the area for a square using two sides
public static int squareArea(int sidea, int sideb) 
{
    int area;
    area = sidea*sideb;
    return area;
}

//Calculate the area of circle with the radius
public static double circleArea(double radius)
{
   double area;
   area = PI * (radius*radius);
   return area;
}

//Calculate the perimeter of circle with the diameter
    public static double circlePerimeter(int diameter)
{
    double perimeter;
    perimeter = PI * diameter;
    return perimeter;
}

//Calculate the perimeter of square with a side
public static double squarePerimeter(double side)
{
    double perimeter;
    perimeter = side * side ;
    return perimeter;
}

//Calculate the volume of the sphere with the radius
public static double sphereVolume(double radius)
{
    double sphere;
    sphere = (4/3) * (PI  * (radius*radius*radius));
    return sphere;
}

//Calculate the area of regular pentagon with one side
public static float pentagonArea(int side)
{
    return 0;
}

//Calculate the Hypotenuse with two cathetus
public static double calculateHypotenuse(double catA, double catB)
{
    return 0.0;
}

}
