package com.sigmotoa.codes.workshop;

import java.util.Scanner;
/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Convertion Exercises
 */
public class Convertion {

    //convert of units of Metric System

//Km to metters
    public static double kmTom(double km)
    {
        double metter;

        metter  = km*1000;

        return metter;
    }
    
    //Km to cm
    public static double kmTocm(double km)
    {
        double cme;

        cme = km*100000;

        return cme;
    }

//milimetters to metters
    public static double mmTom(int mm)
    {

        double metter;
        metter = mm/1000.0;
        return metter;
    }
//convert of units of U.S Standard System

//convert miles to foot
    public static double milesToFoot(double miles)
    {
        double foots;
        foots = miles * 5280;
        return foots;
    }
//convert yards to inches
    public static int yardToInch(int yard)
    {
        int inch = 0;
        inch = yard * 3;
        //inch = yard * 36; validar con el profesor
        return inch;
    }
    
    //convert inches to miles
    public static double inchToMiles(double inch)
    {
        double miles = 0;
        miles = inch / 63360;
        return miles;
    }
//convert foot to yards
    public static int footToYard(int foot)
    {
        int yards = 0;
        yards = foot / 3;
        return yards;
    }

//Convert units in both systems

//convert Km to inches
    public static double kmToInch(String km)
    {
        double inch = 0.0;
        inch = Double.parseDouble(km)* 39370.079;
        return  inch;
    }

//convert milimmeters to foots
    public static double mmToFoot(String mm)
    {
        double foot = 0;
        foot = Double.parseDouble(mm)/ 304.8;
        return foot;
    }
//convert yards to cm    
    public static double yardToCm(String yard)
    {
        double cm = 0;
        cm = Double.parseDouble(yard) * 91.44;
        return cm;
    }


}
